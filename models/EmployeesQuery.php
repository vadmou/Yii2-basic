<?php

namespace app\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[employees]].
 *
 * @see employees
 */
class employeesQuery extends \yii\db\ActiveQuery
{

    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return employees[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return employees|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
