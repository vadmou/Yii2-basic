<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m161028_141435_create_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employees', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'tree' => $this->integer()->notNull()->defaultValue(1),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'position' => $this->string()->notNull()->defaultValue(0),
            'surname' => $this->string()->notNull()->defaultValue(0),
            'email' => $this->string()->notNull()->defaultValue(0),
            'phone' => $this->integer()->notNull()->defaultValue(0),
            'note' => $this->string()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employees');
    }
}
