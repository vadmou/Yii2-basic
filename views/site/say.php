<?php
/**
 * Created by PhpStorm.
 * User: darth
 * Date: 9/25/2016
 * Time: 9:04 PM
 */

use yii\helpers\Html;
?>

<h1>Hello <?= Html::encode($target) ?></h1>
<p>Welcome to your first Yii2 application</p>