<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Employees;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['type' => 'string']) ?>

    <div class='form-group field-attribute-parentId'>
        <?= Html::label('Руководитель', 'parent', ['class' => 'control-label']);?>
        <?= Html::dropdownList(
            'Employees[parentId]',
            $model->parentId,
            Employees::getTree($model->id),
            ['prompt' => 'Не выбран', 'class' => 'form-control']
        );?>

    </div>

    <?= $form->field($model, 'position')->textInput(['type' => 'string']) ?>

    <?= $form->field($model, 'email')->textInput(['type' => 'string']) ?>

    <?= $form->field($model, 'phone')->textInput(['type' => 'string']) ?>

    <?= $form->field($model, 'note')->textInput(['type' => 'string']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>