<?php
use yii\helpers\Html;
use yii\authclient\widgetsdebug;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Персонал'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Вернуться'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php


    //echo "<pre>";
    //print_r($find);
    //echo "</pre>";

    ?>

    <ul>
        <ul>
            <ul>
                <?php
                $oldItem = NULL;
                $lavel = 0;
                foreach ($find as $item) {
                if (!is_null($oldItem) && $oldItem['rgt'] > $item['lft']) {
                    $lavel++;
                } else if (!is_null($oldItem) && $oldItem['rgt'] + 1 < $item['rgt']) {
                    $step = $item['lft'] - ($oldItem['rgt'] + 1);
                    $lavel -= $step;
                    echo str_repeat('</ul></li>', $step);
                }

                if ($item['rgt'] - $item['lft'] == 1) { ?>
                    <li><?php echo $item['name'] . ' ' . $item['surname']; ?> </li>

                <?php } else {//parent
                ?>
                <li><?php echo $item['name'] . ' ' . $item['surname']; ?>
                    <ul>
                        <?php }

                        $oldItem = $item;
                        }
                        if ($lavel > 0) {
                            echo str_repeat('</ul></li>', $lavel);
                        }
                        ?>
